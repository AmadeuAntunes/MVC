﻿using MVC.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVC.Controllers
{
    public class HomeController : Controller
    {

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Menu()
        {
            return View();
        }

        public ActionResult Local()
        {
            return View();
        }

        public ActionResult Admin()
        {
            return View();
        }

        public ActionResult Reservas()
        {
            return View();
        }


        [HttpPost]
        public ActionResult Reservas(string nome, string email, int numero, string datetime )
        {
            DataClasses1DataContext db = new DataClasses1DataContext();

            Table t = new Table
            {
                nome = nome,
                email = email,
                numero = numero,
                DataTime = Convert.ToDateTime(datetime)
            };
      
             db.Tables.InsertOnSubmit(t);
             db.SubmitChanges();
            
            return View();  
        }

        public ActionResult ConsultarReservas()
        {
            DataClasses1DataContext db = new DataClasses1DataContext();
            var tabelas = db.Tables.ToList();
            ViewData["MyData"] = tabelas;
            return View();
        }

    }
}